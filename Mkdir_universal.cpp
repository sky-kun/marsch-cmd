/*

	Mkdir_universal.cpp
	Universelle Verzeichnisfunktionen.
	


*/

#ifdef __linux__
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <ctime>
#include "global_functions.hpp"

using namespace std;

bool DirExists(const char* DirName) {

	struct stat Kb;

	if (stat(DirName, &Kb) == 0 && S_ISDIR(Kb.st_mode))
	{
		//Hier ist rausgefunden worden das das Verzeichnis existiert.
		return true;
	}
	//Verzeichnis existiert nicht.
	return false;
}

bool HasEnding (string const &fullString, string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}


bool CreateDirInHome(const string &DirName, const string &HomeName) {
	//Zeitparameter für Fehlerlog
	time_t Zeitstempel;
    tm *nun;
    Zeitstempel = time(0);
	nun = localtime(&Zeitstempel);

	string strTemp = "";
	
	if (!HasEnding(HomeName, "/")) {
		strTemp = HomeName + "/" + DirName;
	}
	else {
		strTemp = HomeName + DirName;
	}
	
	int numLength = strTemp.length(); 
	char charTemp[1024];
	strcpy(charTemp, strTemp.c_str()); 


	//Schauen ob Verzeichnis überhaupt existiert...
	//Wenn ja braucht ja nix angelegt zu werden.
	if (DirExists(charTemp) == true) return true;
	
	const int dir_err = mkdir(charTemp, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		if (-1 == dir_err)
		{
			cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Warnung: Fehler beim Erstellen des Verzeichnisses, wahrscheinlich ist dieses bereits vorhanden." << endl;
			//return false;

		}
		
	//In das Verzeichnis wechseln
	if(chdir(charTemp) == -1) {
      		cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Konnte nicht in das Verzeichnis wechseln" << endl;
      		return false;
	}
	else {
		cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Erfolgreich nach " << strTemp << " gewechselt." << endl;
		//Testen ob Datei erstellt wird
		//Temporäres Char
		const char charTestdatei[] = {"test"}; 
		FILE * pFile;
		pFile = fopen("test", "w");
		if (pFile!=NULL) {
			
			fclose(pFile);
			cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Testdatei erfolgreich angelegt, es wird davon ausgegangen das der Ordner beschreibbar ist." << endl;
			remove(charTestdatei);
			return true;
		}
		else {
			cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] FEHLER: Testdatei konnte nicht angelegt werden, kein Schreibrecht in " << strTemp << endl;
			return false;
		}
	}
	
	
}




#elif _WIN32
#include <string>
#include <windows.h>
#include <iostream>
#include <cstdlib>
#include <io.h>
#include <ctime>
#include "global_functions.hpp"

using namespace std;

// string ValUhrzeit(int numUhrzeit) {
	// string strUhrzeit = "";
	// stringstream strStream;
	
	
	
	// switch(numUhrzeit) 
	// {
		// case 1:
		// case 2:
		// case 3:
		// case 4:
		// case 5:
		// case 6:
		// case 7:
		// case 8:
		// case 9:
			
			// strStream << "0" << numUhrzeit;
			// break;
			
		// default:
			// strStream << numUhrzeit;
			
			// break;
		
		
	// }
	
	// strUhrzeit = strStream.str();
	
	// return strUhrzeit;
	
// }

bool DirExists(const char* DirName) {
  DWORD attribs = ::GetFileAttributesA(DirName);
  if (attribs == INVALID_FILE_ATTRIBUTES) {
    return false;
  }
  return true;
}

bool HasEnding (string const &fullString, string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

bool CreateDirInHome(const string &DirName, const string &HomeName) {
  
	//Zeitparameter für Fehlerlog
	time_t Zeitstempel;
    tm *nun;
    Zeitstempel = time(0);
	nun = localtime(&Zeitstempel);
  
	string strTemp = "";
	
	if (!HasEnding(HomeName, "\\")) {
		strTemp = HomeName + "\\" + DirName;
	}
	else {
		strTemp = HomeName + DirName;
	}
	
	int numLength = strTemp.length(); 
	
	
	
	//char* charTemp() = strTemp;
	
	char charTemp[1024];
	strcpy(charTemp, strTemp.c_str()); 
  
  
	if(!CreateDirectory(charTemp, NULL)) {
		cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Warnung: Fehler beim Erstellen des Verzeichnisses, wahrscheinlich ist dieses bereits vorhanden." << endl;
		//return false;
	}

	//In das Verzeichnis wechseln
	if(chdir(charTemp) == -1) {
      cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Konnte nicht in das Verzeichnis wechseln" << endl;
      return false;
	}
	else {
		cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Erfolgreich nach " << strTemp << " gewechselt." << endl;
		//Testen ob Datei erstellt wird
		//Temporäres Char
		const char charTestdatei[] = {"test"}; 
		FILE * pFile;
		pFile = fopen("test", "w");
		if (pFile!=NULL) {
			
			fclose(pFile);
			cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Testdatei erfolgreich angelegt, es wird davon ausgegangen das der Ordner beschreibbar ist." << endl;
			remove(charTestdatei);
			return true;
		}
		else {
			cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] FEHLER: Testdatei konnte nicht angelegt werden, kein Schreibrecht in " << strTemp << endl;
			return false;
		}
		
		
	}
  }
#else

#endif



