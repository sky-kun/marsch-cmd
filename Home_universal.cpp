/*

	home_universal.cpp
	
	


*/

#ifdef __linux__
#include <string> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
using namespace std;

string GetHome() {
        char *homedir = getenv("HOME");

        if (homedir != NULL) {
                //cout << "Heimverzeichnis" << endl;
                //cout << "%s\n" << homedir << endl;
				return homedir;
        }

        uid_t uid = getuid();
        struct passwd *pw = getpwuid(uid);

        if (pw == NULL) {
                //cout << "Failed\n";
                return "";
        }

        //cout << "%s\n", pw->pw_dir;

        return pw->pw_dir;
}


#elif _WIN32
#include <string>
#include <windows.h>
#include <shlobj.h>
using namespace std;


string GetHome()
  {
  
  char path[ MAX_PATH ];
  if (SHGetFolderPathA( NULL, CSIDL_PROFILE, NULL, 0, path ) != S_OK)
    {
    // cout << "Leider nicht möglich!\n";
	return "";
    }
  else
    {
    return path;
	
    }

  // if (SHGetFolderPathA( NULL, CSIDL_LOCAL_APPDATA, NULL, 0, path ) != S_OK)
    // {
    // cout << "Leider nicht möglich!\n";
    // }
  // else
    // {
    // cout << "AppData Verzeichnis = \"" << path << "\"\n";
    // }

  // return 0;
  }
#else

#endif



