#ifndef MKDIR_UNIVERSAL_H
#define MKDIR_UNIVERSAL_H

bool DirExists(const char* DirName);
bool HasEnding (string const &fullString, string const &ending);
bool CreateDirInHome(const string &DirName, const string &HomeName);

#endif /* MKDIR_UNIVERSAL_H */
