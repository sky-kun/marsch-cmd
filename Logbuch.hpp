#ifndef LOGBUCH_HPP
#define LOGBUCH_HPP
#include <string>
#include <fstream>
#include <vector>
#include "Datum.hpp"

using namespace std;

	class Konfiguration {
		protected:
			/** Name der sendenden Station. */
			string NameSender; 
			/** Zeitzone der Station. */
			string Timezone; 
			/** QTH der sendenden Station. */
			string QTHsender; 
			/** Name der sendenden Person. */
			string CallSender;
			/** Geladene Einstellungen in einem String Vector */
			vector<string> Einstellungspuffer; 
		public:
		
		Konfiguration(string NameSender="", string Timezone="", string QTHsender="");
		virtual ~Konfiguration();
		
		//Getter
		vector<string> GetEinstellungspuffer() const;
		string GetCallSender() const;
		string GetNameSender() const;
		
		//setter
		void SetEinstellungspuffer(vector<string> oNewPuffer);
		virtual void SetCallSender(string strNewValue);
		virtual void SetNameSender(string strNewValue);
		
		//Weitere Methoden
		vector<string> Split(string str, string sep);
		string GetDate() const;
		Datum GetDate(bool bolWithYear) const;
		string Now() const;
		string NowUTC() const;
		Datum NowUTC(bool withDate) const;
		string DXCCToCountry(const string &CallDXCT) const;
		void CharArrayUpper(char arr[]);
		int LenghOfCharArr(char str[]);
	};
	
	class Logbucheintrag : public Konfiguration {
		private:
			string CallReceiver;
			float TRXFrequency;
			float RECVFrequency;
			Datum DateUTC;
			string Mode;
			string Comment;
			string NameReceiver;
			string QRG;
			string QSLvia;
			string Operator;
			string QTH;
		public:
			Logbucheintrag();
			Logbucheintrag(string CallReceiver, float TRXFrequency, float RECVFrequency, Datum DateUTC);
			Logbucheintrag(string CallReceiver, float TRXFrequency, float RECVFrequency, Datum DateUTC, string Mode, string Comment="", string NameReceiver="", string QRG="", string QSLvia="", string Operator="", string QTH="");
			
			//Setter
			void SetNameReceiver(string strNewValue);
			void SetTRXFrequency(float numNewValue);
			void SetRECVFrequency(float numNewValue);
			void SetCallReceiver(string strNewValue);
			void SetDateUTC(Datum dateNewValue);
			void SetDateUTC(string strUhrzeit, int numYear, int numTag, int numMonat);
			void SetQTH(string strNewValue);
			void SetQSLvia(string strNewValue);
			void SetOperator(string strNewValue);
		
			//Getter
			
			float GetTRXFrequency() const;
			float GetRECVFrequency() const;
			Datum GetDateUTC() const;
			string GetCallReceiver() const;
			string GetQTH() const;
			string GetOperator() const;
			string GetQSLvia() const;
			
			
			//Weitere Methoden
			void Print();
			string PrintDateUTC() const;
	};
	
	class Logbuch : public Konfiguration {
		private:
		/** Dateiname des Logbuches */
		string Filename;
		/** Filestream des Logbuches, ist 0 bzw NULL, falls keien Datei geladen wurde. */
		fstream f;
		/** Geladene bzw. erzeugte Einträge des Logbuches im RAM. */
		vector<Logbucheintrag> Entries;
		/** Anzahl der Einträge im Logbuch. */
		long Count;
		/** Kapazität des Logbuches. */
		long Capacity;
		
		
		public:
		//Constructors
		Logbuch(long Capacity);
		//Logbuch(string Filename);
		Logbuch(string Filename, long Capacity);                                                                                         
		//Logbuch(string Filename, string CallSender, string CallReceiver, float TRXFrequency, Datum DateUTC, string Mode, string Comment="", string NameReceiver="", string QRG="", string NameSender="", string QSLvia="", string Operator="");
		//Destructors
		~Logbuch();
		
		//Getter
		string GetFilename() const;
		long GetCapacity();
		long GetCount();
		vector<Logbucheintrag> GetEntries();
  
		//Weitere Methoden
		//void Print() const;
		bool SaveLogbook();
		bool SaveLogbook(string strFilename);
		bool LoadLogbook(string strFilename);
		bool Add(const Logbucheintrag &Eintrag);
		template<typename... Args> bool Add(Args...);
		
	};
	

#endif