#                    ___====-_  _-====___
#              _--^^^#####//      \\#####^^^--_
#           _-^##########// (    ) \\##########^-_
#          -############//  |\^^/|  \\############-
#        _/############//   (@::@)   \\############\_
#       /#############((     \\//     ))#############\
#      -###############\\    (oo)    //###############-
#     -#################\\  / VV \  //#################-
#    -###################\\/      \//###################-
#   _#/|##########/\######(   /\   )######/\##########|\#_
#   |/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
#   `  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
#      `   `  `      `   / | |  | | \   '      '  '   '
#                       (  | |  | |  )
#                      __\ | |  | | /__
#                     (vvv(VVV)(VVV)vvv)
# 
# Makefile
#
# Copyright (c) 2018 Ayron
#


# Edit from here

TCPREFIX = 
CXX = $(TCPREFIX)clang++
CC = $(TCPREFIX)clang
AS = $(TCPREFIX)as
AR = $(TCPREFIX)ar
LD = $(TCPREFIX)ld
OBJCOPY = $(TCPREFIX)objcopy
OBJDUMP = $(TCPREFIX)objdump
HOSTCC = clang

### STOP ### STOP ### STOP ### STOP ###

DFLAGS = 
OFLAGS = -O3
LFLAGS = -g
LIBS = 
CFLAGS = -std=gnu89 -g
CXXFLAGS = -std=gnu++11 -g
WFLAGS = -Wall -Wextra -Wno-comment -Wno-implicit-int -Wno-implicit-function-declaration -Wno-unused-variable -Wno-unused-function -Wno-unused-parameter
CFLAGS += $(OFLAGS) $(DFLAGS) $(WFLAGS)
CXXFLAGS += $(OFLAGS) $(DFLAGS) $(WFLAGS)

OBJC = 
OBJS = 
OBJCXX = Main.o \
	Initialisierung.o \
	Logbuch.o \
	Datum.o \
	global_functions.o \
	Mkdir_universal.o \
	Home_universal.o \
	Hauptauswahl.o

EXEC = marsch-cmd
DIS = marsch-cmd.dis

exec: $(EXEC)

all: $(EXEC) $(DIS)

clean:
	rm -f $(OBJC) $(OBJCXX) $(OBJS) $(EXEC) $(DIS)

distclean:
	rm -f $(OBJC) $(OBJS) $(EXEC) $(DIS) 
dis: $(DIS)

$(DIS): $(EXEC)
	@echo [DISASM] $(EXEC)
	@$(OBJDUMP) -d $(EXEC) > $(DIS)

$(EXEC): $(OBJS) $(OBJC) $(OBJCXX)
	@echo [LD] $@
	@$(CXX) $(LFLAGS) -o $(EXEC) $(OBJS) $(OBJC) $(OBJCXX) $(LIBS)

%.o: %.s
	@echo [AS] $@
	@$(AS) $(SFLAGS) -o $@ $<

%.o: %.c
	@echo [CC] $@
	@$(CC) $(CFLAGS) -march=$(ARCH) -o $@ -c $<

%.o: %.cpp
	@echo [CXX] $@
	@$(CXX) $(CXXFLAGS) -march=$(ARCH) -o $@ -c $<
