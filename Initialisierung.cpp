#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <streambuf>
#include <string>
#include <exception>
#include <stdexcept>
#include "Home_universal.hpp"
#include "Logbuch.hpp"
#include "Mkdir_universal.hpp"
#include "Hauptauswahl.hpp"
#include "global_functions.hpp"
#include "Own_Exception.hpp"

using namespace std;

/** Initialisierung von marsch-cmd.
*/
int Start(int argc, char *argv[]) {
	
	try {

	stringstream Einstellungspuffer;	
	//Zeitparameter für Fehlerlog
	time_t Zeitstempel;
    tm *nun;
    Zeitstempel = time(0);
	nun = localtime(&Zeitstempel);
	
	cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Heimverzeichnis wird initialisiert..." << endl;
	//Heimverzeichnis initialisieren
	string Home = GetHome();
	
	if (Home == "") throw Main_Exception("Fehler: Heimverzeichnis gab einen Leerstring zurück. Darf aber nicht leer sein!");
	cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Heimatverzeichnis initialisiert." << endl;
	//Schauen ob .marsch_cpp Verzeichnis vorhanden ist, erstelle
	//das Verzeichnis eventuell und Wechsel den Arbeitsordner in das Verzeichnis.
	if (CreateDirInHome(".marsch_cmd", Home) == false) throw Main_Exception("Fehler: Beim Erstellen/Setzen des Arbeitsordners trat ein Fehler auf.");
	//Konfigurationsdatei wird geöffnet
	cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Konfiguration wird geladen..." << endl;
	
	FILE *konfigdatei;

	konfigdatei = fopen("marsch-cmd.md", "r");
	

	if (konfigdatei == NULL) {

		cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Warnung: Konnte die Konfigurationsdatei nicht laden, es wird davon ausgegangen das diese nicht existiert. Es wird nichts geladen..." << endl;
	//fclose(konfigdatei);
    //cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Datei wurde geschlossen." << endl;

	}
	else {
		fclose(konfigdatei);
		cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Datei existiert." << endl;
		//ToDo: Hier falls die Datei gelesen werden kann, diese Config auslesen.
		ifstream Einstellungsdatei("marsch-cmd.md");
		
		
		//Daten in den stringstream Einstellungspuffer schreiben.
		Einstellungspuffer << Einstellungsdatei.rdbuf();
		
		
	}
	
	cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Klassenerstellung wird initialisiert..." << endl;
	//nun können wir die Klasse neu erstellen
	//Logbuch wird mit Standard-Konstruktor angelegt.
	cout << "[" << ValUhrzeit(nun->tm_hour) << ':' << ValUhrzeit(nun->tm_min) << "] Logbuch wird geladen..." << endl;
	//Standard-Logbuch wird geladen
	Logbuch InitLog("marsch-cmd.log", 1000);
	
	//Konfiguration parsen
	InitLog.SetEinstellungspuffer(InitLog.Split(Einstellungspuffer.str(), "\n"));
	
	//Nur zum testen...
	//vector<string> tempPuffer = InitLog.GetEinstellungspuffer();
	
	//cout << testPuffer[0] << endl;
	//cout << tempPuffer[0] << endl;
	//cout << tempPuffer[1] << endl;
	
	//Einstellungen setzen/festlegen
	for (const string& i : InitLog.GetEinstellungspuffer()) {
		
		if(i.find("Name:") != string::npos) InitLog.SetNameSender(i.substr(i.find("Name:") + 6));
		if(i.find("Call:") != string::npos) InitLog.SetCallSender(i.substr(i.find("Call:") + 6));
		//if(i.find("QTH:") != string::npos) InitLog.SetQTHSender(i.substr(i.find("QTH:") + 5));
		
	}
	
	
	//Hauptmenü aufrufen
	if (Hauptauswahl(InitLog) != 0) throw Menu_Exception("Fehler: Bei der Ausführung einer Funktion im Hauptmenü trat ein Fehler auf.");
	else return 0;
	
	
	return 0;
	
	}
	
	catch (const Main_Exception& e) {
		
		cout << "Laufzeitfehler aufgetreten: " << e.what() << endl;
		cout << "Programm muss beendet werden.";
		return 1;
	
	
	}
	catch (const Menu_Exception& e) {
			//Standard-Logbuch wird geladen
			Logbuch InitLog("marsch-cmd.log", 1000);
			
			Hauptauswahl(InitLog); //Hauptauswahl wieder aufrufen...
			return 0;
	}
	
}



