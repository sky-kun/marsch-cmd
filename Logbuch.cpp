#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <ctime>
#include <typeinfo>
#include <string.h>
#include <stdio.h>
#include "Datum.hpp"
#include "Logbuch.hpp"
using namespace std;

		// private:
		// string CallSender;
		// string NameSender;
		// float TRXFrequency;
		// string CallReceiver;
		// Datum DateUTC;
		// string Mode;
		// float RECVFrequency;
		// string Comment;
		// string NameReceiver;
		// string QRG;
		// string QSLvia;
		// string QTH;
		// string Operator;
		//Logbuch:
		// string Filename;
		// fstream f;
		// long Count;
		//fstream Einstellungspuffer;
		
		/** Hier wird das Logbuch geschlossen, sonst ist nichts weiter zu tun.
		*/
		Logbuch::~Logbuch(){
			
			/**Objekt auf dem Heap freigeben, aber nur falls es existiert.*/
			//if (Entries != NULL) delete[] Entries;
			f.close();
			//delete[] Entries;
		}
		/** Default-Konstruktor. Logbuch wird ohne Pfad aufgerufen.
			Es wird keine Datei geöffnet.
			
		*/
		Logbuch::Logbuch(long Capacity) : Capacity(Capacity) {
			//Einträge initialisieren.
			//Entries = new Logbucheintrag[Capacity];
			
			//kapazität reservieren.
			Entries.reserve(Capacity);
			
			//Größe ermitteln
			Count = Entries.size();
			
			
		}
		/** Logbuch wird aus angegebenen Pfad aufgerufen.
			Es wird die angegebene Datei geöffnet.
			
		*/
		Logbuch::Logbuch(string Filename, long Capacity) : Filename(Filename), Capacity(Capacity) {
			//Einträge initialisieren.
			//Entries
			//Datei wird geöffnet...
			try {
				vector<string> tempVector;
				string tempString = "";
				f.open(Filename, ios::in);
				char charLine[512]; //512 Zeichen pro Line auslesen...
				
				//Entries müssen geladen werden, vorher muss geschaut werden wie viele Lines die Datei hat...
				long numCounter = 0;
				//In dieser Schleife wird die Anzahl der Zeilen herausgefunden und die Daten gelesen.
				while (!f.eof())
				{
					
					f.getline(charLine, sizeof(charLine));
					numCounter = numCounter + 1;
					//cout << numCounter << endl;
					//String das CharArray zuweisen.
					tempString = charLine;
					
					
					//Nun die Zeile in den Vektor Parsen
					if (tempString != "") {
						//Hier muss die Zeile in einen String geparst werden...
						tempString = charLine;
						tempVector = Split(tempString, ",");
					}
					// Tabelle TRX Frequenz			Empfangsfrequenz			DatumUTC						Rufzeichen Empfangende Station	Rufzeichen sendende Station
					// Puffer << B.GetTRXFrequency() << "," << B.GetRECVFrequency() << "," << B.PrintDateUTC() << "," << B.GetCallReceiver() << "," << B.GetCallSender() << "\n";
					
					if (tempVector.size() > 0) {
						//Ja es sind Einträge drin...
						string::size_type pSize;
						Logbucheintrag tempLogbucheintrag; 
						
						tempLogbucheintrag.SetTRXFrequency(stof(tempVector[0], &pSize)); 
						tempLogbucheintrag.SetRECVFrequency(stof(tempVector[1], &pSize)); 
						
						//ToDo: Datum aus String bilden...
						cout << "------------" << endl;
						cout << tempVector[0] << endl;
						cout << tempVector[1] << endl;
						cout << tempVector[2] << endl;
					}
					if (f.bad()) {
						break;
					}
					//Ich weiß zwar nicht warum eof() bei leeren Dateien nicht auftritt aber dann ist das notwendeig...
					if (numCounter > 10000000) {
						numCounter = 0;
						break;
					}
				}
				if (numCounter == 0) cout << "Logbuch ist noch leer.";
				//Anzahl Einträge eintragen
				Count = Entries.size();
				
				
			}
			catch(ifstream::failure e) {
						cout << "Laufzeitfehler beim öffnen aufgetreten: " << e.what() << endl;
						cout << "Logbuch funktioniert eventuell nicht richtig.";
				
			}
			catch(const exception& e) {
						cout << "Laufzeitfehler beim erstellen der Klasse Logbuch aufgetreten: " << e.what() << endl;
						cout << "Logbuch funktioniert eventuell nicht richtig.";
				
			}
		
		}
		
		/** Getter Methode. Es wird ein vektor vom Typ Logbucheintrag ausgegeben der den Property Entries entspricht.
			
		*/
		vector<Logbucheintrag> Logbuch::GetEntries() {
			
			return Entries;
			
		}
		
		/** Diese Methode addiert ein Logbucheintrag Objekt zum Array vom Typ Logbucheintrag[] und speichert dann das Logbuch. Ziel ist: Logbucheintrag *Entries;
			
		*/
		bool Logbuch::Add(const Logbucheintrag &Eintrag /**< [in] Ein Objekt der Klasse Logbucheintrag der dem Vektor hinzugefügt wird. */) {
				bool bolErgebnis = false;
				/** Referenz muss auf Entries in der Logbuch Klasse angelegt werden. */
				//vector<Logbucheintrag> &Referenz = *Entries; Kann man sich sparen
				

				/** Die Größe vom Array wird erhöht und der Eintrag eingefügt.*/
				//Referenz.resize(numSize + 1); Kann man sich bei push_back sparen
				Entries.push_back(Eintrag);
				
				//Größe ermitteln
				Count = Entries.size();
				
				
				//Logbuch wird gespeichert...
				bolErgebnis = SaveLogbook();
				
				
				if (bolErgebnis == false) return false;
				return true;
			
		}
		/** Diese Methode addiert eine beliebige Anzahl vom Typ Logbucheintrag zum Array vom Typ Logbucheintrag[]. Ziel ist: Logbucheintrag *Entries;
			
		*/
		template<typename... Args> 
		bool Logbuch::Add(Args...) {
			
			
			return true;
		}
		
		//Logbucheintrag
		Logbucheintrag::Logbucheintrag() {}
		//Logbucheintrag(string CallReceiver, float TRXFrequency, float RECVFrequency, Datum DateUTC);
		Logbucheintrag::Logbucheintrag(string CallReceiver, float TRXFrequency, float RECVFrequency, Datum DateUTC) : CallReceiver(CallReceiver), TRXFrequency(TRXFrequency), RECVFrequency(RECVFrequency) , DateUTC(DateUTC) {}
		//Logbucheintrag(string CallReceiver, float TRXFrequency, float RECVFrequency, Datum DateUTC, string Mode, string Comment="", string NameReceiver="", string QRG="", string QSLvia="", string Operator="", string QTH="");
		Logbucheintrag::Logbucheintrag(string CallReceiver, float TRXFrequency, float RECVFrequency, Datum DateUTC, string Mode, string Comment, string NameReceiver, string QRG, string QSLvia, string Operator, string QTH) 
		: CallReceiver(CallReceiver), TRXFrequency(TRXFrequency), RECVFrequency(RECVFrequency), DateUTC(DateUTC), Mode(Mode), Comment(Comment), NameReceiver(NameReceiver), QRG(QRG), QSLvia(QSLvia), Operator(Operator), QTH(QTH)
		{}
		
		/** Logbucheintrag wird auf der Konsole ausgegeben.
			
		*/
		void Logbucheintrag::Print()  {
			
			cout << "Name Sender: " << NameSender << endl;
			cout << "Call Sender: " << CallSender << endl;
			cout << "Call Receiver" << CallReceiver << endl;
			cout << "TRX Frequenz: " << TRXFrequency << endl;
			cout << "Recv Frequenz: " << RECVFrequency << endl;
			cout << "Datum UTC der Unterhaltung: " << DateUTC.GetDatum() << endl;

		
		}
		
		//Setter
			/** Setter Methode. Der std::fstream muss hier als Argument übergeben werden.
			Dies ist normalerweise die Einstellungsdatei.
			ACHTUNG: Übergebene Daten werden nicht validiert.
			
		*/
		void Konfiguration::SetEinstellungspuffer(vector<string> oNewPuffer /**< [in] Ein Array der Klasse std::vector der die neuen Einstellungen von marsch-cmd beinhalten soll. */) {
			
			Einstellungspuffer = oNewPuffer;
		}
		
		/** Setter Methode. Der Name der zu Sendenen-Station muss hier als Argument übergeben werden.
			ACHTUNG: Übergebene Daten werden nicht validiert.
			
		*/
		void Konfiguration::SetNameSender(string strNewValue) {
			
			NameSender = strNewValue;
		}
		/** Setter Methode. Die Frequenz der sendenden Station muss angegeben werden.
			ACHTUNG: Übergebene Daten werden nicht validiert.
			
		*/
		void Logbucheintrag::SetTRXFrequency(float numNewValue) {
			
			TRXFrequency = numNewValue;
		}
		/** Setter Methode. Die Frequenz der empfangenden Station muss angegeben werden.
			ACHTUNG: Übergebene Daten werden nicht validiert.
			
		*/
		void Logbucheintrag::SetRECVFrequency(float numNewValue) {
			
			RECVFrequency = numNewValue;
			
		}
		/** Setter Methode. Der Call der zu Sendenen Station muss hier als Argument übergeben werden.
			ACHTUNG: Übergebene Daten werden nicht validiert.
			
		*/
		void Konfiguration::SetCallSender(string strNewValue) {
			
			CallSender = strNewValue;
		
		}
		
		/** Setter Methode. Der Call der zu Empfangenden Station muss hier als Argument übergeben werden.
			ACHTUNG: Übergebene Daten werden nicht validiert.
			
		*/
		void Logbucheintrag::SetCallReceiver(string strNewValue) {
			
			CallReceiver = strNewValue;
		
		}
		/** Setter Methode. QTH der zu Empfangenden Station muss hier als Argument übergeben werden.
			Hinweis: QTH Sender ist noch nicht implementiert.
			ACHTUNG: Übergebene Daten werden nicht validiert.
			
		*/
		void Logbucheintrag::SetQTH(string strNewValue) {
			
			QTH = strNewValue;
		
		}
		/** Setter Methode. Das Datum der Verbindung in UTC wird gesetzt. Erwartet ein Objekt der Klasse Datum.
		*/
		void Logbucheintrag::SetDateUTC(Datum dateNewValue){
			
			DateUTC = dateNewValue;
			
		}
		/** Setter Methode. Das Datum der Verbindung in UTC wird gesetzt. Erwartet ein Objekt der Klasse String.
		*/
		void Logbucheintrag::SetDateUTC(string strUhrzeit, int numYear, int numTag, int numMonat){
			
			vector<string> Resultate = Split(strUhrzeit, ":");
			
			Datum tempDatum{numTag, numMonat, numYear, strUhrzeit};
			
			
			
			
			DateUTC = tempDatum;
			
		}
		//Getter
		/** Getter Methode. Der vector Einstellungspuffer wird zurückgegeben.
			
		*/
		vector<string> Konfiguration::GetEinstellungspuffer() const {
			
			return Einstellungspuffer;
			
		}
		/** Getter Methode. Call der zu Sendenden-Station wird zurückgegeben.
			
		*/
		string Konfiguration::GetCallSender() const {
		
			return CallSender;
		
		}
		/** Getter Methode. Name der zu Sendenden-Station wird zurückgegeben.
			
		*/
		string Konfiguration::GetNameSender() const {
		
			return NameSender;
		
		}
		/** Getter Methode. Call der zu Empfangenden-Station wird zurückgegeben.
			
		*/
		float Logbucheintrag::GetRECVFrequency() const {
			
			
			return RECVFrequency;
		}
		/** Getter Methode. Call der zu Sendenden-Station wird zurückgegeben.
			
		*/
		float Logbucheintrag::GetTRXFrequency() const {
			
			return TRXFrequency;
			
		}
		
		/** Getter Methode. Call der zu Empfangenden-Station wird zurückgegeben.
			
		*/
		string Logbucheintrag::GetCallReceiver() const {
		
			return CallReceiver;
		
		}
		/** Getter Methode. QTH der zu Empfangenden Station wird zurückgegeben.
			Hinweis: QTH Sender ist noch nicht implementiert.
			ACHTUNG: Übergebene Daten werden nicht validiert.
			
		*/
		string Logbucheintrag::GetQTH() const {
		
			return QTH;
		
		}
		/** Getter Methode. Das Datum der Verbindung in UTC wird zurückgegeben.
			
		*/
		string Logbucheintrag::PrintDateUTC() const {
		
			return DateUTC.GetDatum();
		
		}
		
		Datum Logbucheintrag::GetDateUTC() const {
		
			return DateUTC;
		
		}
		/** Getter Methode. Sendeweg der QSL-Karte wird zurückgegeben.
			Beispiel: QSL via bureau
			
		*/
		string Logbucheintrag::GetQSLvia() const {
		
			return QSLvia;
		
		}
		/** Getter Methode. Dateiname wird zurückgegeben. Der vollständige Pfad ist zur
			Wahrung der Kompatibilität generell nicht in der Klasse Logbuch eingebettet.
			So wird sichergestellt das beispielsweise
			ein Logbuch was unter Linux erstellt wurde auch
			auf anderen Systemen geöffnet werden kann.
			
			Dies funktioniert nur wenn sich das Logbuch im Unterordner
			.marsch-cmd im home Verzeichnis des Benutzers befindet.
			Dieses wird jedoch beim Laden von marsch-cmd automatisiert
			erledigt.
			@return Dateiname ohne Pfad.
			
		*/
		string Logbuch::GetFilename() const {
			
			return Filename;
		}
		
		//Weitere Methoden
		/** Gibt die Länge eines Charr Arrays zurück.
			@return Länge des Char Arrays als Integer.
			
		*/
		int Konfiguration::LenghOfCharArr(char str[]) {
			int counter = 0;
	
			while(str[counter] != '\0') counter++;
	
			return counter;
	
		}
		/** Schreibt alle Zeichen in Großbuchstaben. Aus abc wird ABC.
			
			
		*/
		void Konfiguration::CharArrayUpper(char arr[]) {
		int numLen = LenghOfCharArr(arr);
		
		
		for (int i = 0;i <= numLen;i++) {
			
			int numTempSwitch = (int)arr[i];
			
			switch(numTempSwitch) {
		
				case 97:
					arr[i] = 'A';
					break;
				case 98:
					arr[i] = 'B';
					break;
				case 99:
					arr[i] = 'C';
					break;
				case 100:
					arr[i] = 'D';
					break;	
				case 101:
					arr[i] = 'E';
					break;					
				case 102:
					arr[i] = 'F';
					break;
				case 103:
					arr[i] = 'G';
					break;			
				case 104:
					arr[i] = 'H';
					break;	
				case 105:
					arr[i] = 'I';
					break;	
				case 106:
					arr[i] = 'J';
					break;
				case 107:
					arr[i] = 'K';
					break;
				case 108:
					arr[i] = 'L';
					break;
				case 109:
					arr[i] = 'M';
					break;
				case 110:
					arr[i] = 'N';
					break;
				case 111:
					arr[i] = 'O';
					break;
				case 112:
					arr[i] = 'P';
					break;
				case 113:
					arr[i] = 'Q';
					break;
				case 114:
					arr[i] = 'R';
					break;
				case 115:
					arr[i] = 'S';
					break;
				case 116:
					arr[i] = 'T';
					break;
				case 117:
					arr[i] = 'U';
					break;
				case 118:
					arr[i] = 'V';
					break;
				case 119:
					arr[i] = 'W';
					break;
				case 120:
					arr[i] = 'X';
					break;
				case 121:
					arr[i] = 'Y';
					break;
				case 122:
					arr[i] = 'Z';
					break;
		}
	}
}
		
		/** Gibt aus einem DXCC Landeskenner ein Land aus. (Kann nur Länder mit 2 Stellen erkennen)
		@return string-objekt mit dem Land das erkannt wurde, andernfalls "".
		*/
		string Konfiguration::DXCCToCountry(const string &CallDXCT /**< [in] String der die ersten beiden Ziffern des Rufzeichens enthält. */) const {
			
			if (CallDXCT == "T6") return "Afghanistan";
			if (CallDXCT == "KL") return "Alaska";
			if (CallDXCT == "ZA") return "Albania";
			if (CallDXCT == "7X") return "Algeria";
			if (CallDXCT == "C3") return "Andorra";
			if (CallDXCT == "D2") return "Angola";
			if (CallDXCT == "V2") return "Antigua, Barbuda";
			if (CallDXCT == "LU") return "Argentinia";
			if (CallDXCT == "EK") return "Armenia";
			if (CallDXCT == "P4") return "Aruba";
			if (CallDXCT == "VK") return "Australia";
			if (CallDXCT == "OE") return "Austria";
			if (CallDXCT == "4J") return "Azerbaijan";
			if (CallDXCT == "CU") return "Azores";
			if (CallDXCT == "C6") return "Bahamas";
			if (CallDXCT == "A9") return "Bahrain";
			if (CallDXCT == "S2") return "Bangladesh";
			if (CallDXCT == "8P") return "Barbados";
			if (CallDXCT == "EV") return "Belarus";
			if (CallDXCT == "ON") return "Belgium";
			if (CallDXCT == "V3") return "Belize";
			if (CallDXCT == "TY") return "Benin";
			if (CallDXCT == "A5") return "Bhutan";
			if (CallDXCT == "CP") return "Bolivia";
			if (CallDXCT == "E7") return "Bosnia-Hercegovina";
			if (CallDXCT == "A2") return "Botswana";
			if (CallDXCT == "3Y") return "Bouvet";
			if (CallDXCT == "PY") return "Brasil";
			if (CallDXCT == "V8") return "Brunei";
			if (CallDXCT == "LZ") return "Bulgaria";
			if (CallDXCT == "XT") return "Burkina Faso";
			if (CallDXCT == "9U") return "Burundi";
			if (CallDXCT == "XU") return "Cambodia";
			if (CallDXCT == "TJ") return "Cameroon";
			if (CallDXCT == "VE") return "Canada";
			if (CallDXCT == "D4") return "Cape Verde";
			if (CallDXCT == "ZF") return "Cayman Island";
			if (CallDXCT == "TL") return "Central African Republic";
			if (CallDXCT == "TT") return "Chad";
			if (CallDXCT == "FK") return "Chesterfield Island";
			if (CallDXCT == "CE") return "Chile";
			if (CallDXCT == "BY") return "China";
			if (CallDXCT == "HK") return "Colombia";
			if (CallDXCT == "D6") return "Comoros";
			if (CallDXCT == "TN") return "Congo";
			if (CallDXCT == "TK") return "Corsica";
			if (CallDXCT == "TI") return "Costa Rica";
			if (CallDXCT == "9A") return "Croatia";
			if (CallDXCT == "CO") return "Cuba";
			if (CallDXCT == "ZC") return "Cyprus SBA";
			if (CallDXCT == "5B") return "Cyprus";
			if (CallDXCT == "OK") return "Czech Republic";
			if (CallDXCT == "9Q") return "Democratic Republic Congo (Zaire)";
			if (CallDXCT == "OZ") return "Denmark";
			if (CallDXCT == "J2") return "Djibouti";
			if (CallDXCT == "J7") return "Dominica";
			if (CallDXCT == "HI") return "Dominican Republic";
			if (CallDXCT == "HC") return "Ecuador";
			if (CallDXCT == "SU") return "Egypt";
			if (CallDXCT == "YS") return "El Salvador";
			if (CallDXCT == "GA") return "England";
			if (CallDXCT == "GB") return "England";
			if (CallDXCT == "GC") return "England";
			if (CallDXCT == "GD") return "England";
			if (CallDXCT == "GE") return "England";
			if (CallDXCT == "GF") return "England";
			if (CallDXCT == "GG") return "England";
			if (CallDXCT == "GH") return "England";
			if (CallDXCT == "GJ") return "England";
			if (CallDXCT == "GK") return "England";
			if (CallDXCT == "GL") return "England";
			if (CallDXCT == "GM") return "England";
			if (CallDXCT == "GN") return "England";
			if (CallDXCT == "GO") return "England";
			if (CallDXCT == "GP") return "England";
			if (CallDXCT == "GQ") return "England";
			if (CallDXCT == "GR") return "England";
			if (CallDXCT == "GS") return "England";
			if (CallDXCT == "GT") return "England";
			if (CallDXCT == "GU") return "England";
			if (CallDXCT == "GV") return "England";
			if (CallDXCT == "GW") return "England";
			if (CallDXCT == "GX") return "England";
			if (CallDXCT == "GY") return "England";
			if (CallDXCT == "GZ") return "England";
			if (CallDXCT == "3C") return "Equatorial Guinea";
			if (CallDXCT == "E3") return "Eritrea";
			if (CallDXCT == "ES") return "Estonia";
			if (CallDXCT == "ET") return "Ethiopia";
			if (CallDXCT == "OY") return "Faroe Island";
			if (CallDXCT == "OH") return "Finland";
			if (CallDXCT == "FA") return "France";
			if (CallDXCT == "FB") return "France";
			if (CallDXCT == "FC") return "France";
			if (CallDXCT == "FD") return "France";
			if (CallDXCT == "FE") return "France";
			if (CallDXCT == "FF") return "France";
			if (CallDXCT == "FG") return "France";
			if (CallDXCT == "FH") return "France";
			if (CallDXCT == "FJ") return "France";
			if (CallDXCT == "FK") return "France";
			if (CallDXCT == "FL") return "France";
			if (CallDXCT == "FM") return "France";
			if (CallDXCT == "FN") return "France";
			if (CallDXCT == "FO") return "French Polynesia";
			if (CallDXCT == "FP") return "France";
			if (CallDXCT == "FQ") return "France";
			if (CallDXCT == "FR") return "France";
			if (CallDXCT == "FS") return "France";
			if (CallDXCT == "FT") return "France";
			if (CallDXCT == "FU") return "France";
			if (CallDXCT == "FV") return "France";
			if (CallDXCT == "FW") return "France";
			if (CallDXCT == "FX") return "France";
			if (CallDXCT == "FY") return "French Guinea";
			if (CallDXCT == "FZ") return "France";
			if (CallDXCT == "TR") return "Gabon";
			if (CallDXCT == "C5") return "Gambia";
			if (CallDXCT == "4L") return "Georgia";
			if (CallDXCT == "DA") return "Germany";
			if (CallDXCT == "DB") return "Germany"; //DB0 Relaisstationen.
			if (CallDXCT == "DC") return "Germany";
			if (CallDXCT == "DD") return "Germany";
			if (CallDXCT == "DE") return "Germany";
			if (CallDXCT == "DF") return "Germany";
			if (CallDXCT == "DG") return "Germany";
			if (CallDXCT == "DH") return "Germany";
			if (CallDXCT == "DI") return "Germany";
			if (CallDXCT == "DJ") return "Germany";
			if (CallDXCT == "DK") return "Germany";
			if (CallDXCT == "DL") return "Germany";
			if (CallDXCT == "DM") return "Germany";
			if (CallDXCT == "DN") return "Germany"; //Ausbildungsrufzeichen
			if (CallDXCT == "DO") return "Germany"; //E-Klasse
			if (CallDXCT == "DP") return "Germany";
			if (CallDXCT == "DQ") return "Germany";
			if (CallDXCT == "DR") return "Germany";
			if (CallDXCT == "9G") return "Ghana";
			if (CallDXCT == "ZB") return "Gibraltar";
			if (CallDXCT == "SV") return "Greece";
			if (CallDXCT == "SX") return "Greece";
			if (CallDXCT == "SY") return "Greece";
			if (CallDXCT == "SZ") return "Greece";
			if (CallDXCT == "J4") return "Greece";
			if (CallDXCT == "OX") return "Greenland";
			if (CallDXCT == "J3") return "Grenada";
			if (CallDXCT == "FG") return "Guadeloupe";
			if (CallDXCT == "TG") return "Guatemala";
			if (CallDXCT == "GU") return "Guernsey";
			if (CallDXCT == "J5") return "Guinea-Bissau";
			if (CallDXCT == "3X") return "Guinea";
			if (CallDXCT == "8R") return "Guyana";
			if (CallDXCT == "HH") return "Haiti";
			if (CallDXCT == "HR") return "Honduras";
			if (CallDXCT == "HA") return "Hungary";
			if (CallDXCT == "4U") return "ITU Geneva";
			if (CallDXCT == "TF") return "Iceland";
			if (CallDXCT == "VU") return "India";
			if (CallDXCT == "VT") return "India";
			if (CallDXCT == "VW") return "India";
			if (CallDXCT == "VV") return "India";
			if (CallDXCT == "AT") return "India";
			if (CallDXCT == "AU") return "India";
			if (CallDXCT == "AV") return "India";
			if (CallDXCT == "AW") return "India";
			if (CallDXCT == "YB") return "Indonesia";
			if (CallDXCT == "EP") return "Iran";
			if (CallDXCT == "YI") return "Iraq";
			if (CallDXCT == "EI") return "Ireland";
			if (CallDXCT == "GD") return "Isle of Man";
			if (CallDXCT == "4X") return "Israel";
			if (CallDXCT == "4Z") return "Israel";
			if (CallDXCT == "IA") return "Italy";
			if (CallDXCT == "IB") return "Italy";
			if (CallDXCT == "IC") return "Italy";
			if (CallDXCT == "ID") return "Italy";
			if (CallDXCT == "IE") return "Italy";
			if (CallDXCT == "IF") return "Italy";
			if (CallDXCT == "IG") return "Italy";
			if (CallDXCT == "IH") return "Italy";
			if (CallDXCT == "II") return "Italy";
			if (CallDXCT == "IJ") return "Italy";
			if (CallDXCT == "IK") return "Italy";
			if (CallDXCT == "IL") return "Italy";
			if (CallDXCT == "IM") return "Italy";
			if (CallDXCT == "IN") return "Italy";
			if (CallDXCT == "IO") return "Italy";
			if (CallDXCT == "IP") return "Italy";
			if (CallDXCT == "IQ") return "Italy";
			if (CallDXCT == "IR") return "Italy";
			if (CallDXCT == "IS") return "Italy";
			if (CallDXCT == "IT") return "Italy";
			if (CallDXCT == "IU") return "Italy";
			if (CallDXCT == "IV") return "Italy";
			if (CallDXCT == "IW") return "Italy";
			if (CallDXCT == "IX") return "Italy";
			if (CallDXCT == "IY") return "Italy";
			if (CallDXCT == "IZ") return "Italy";
			if (CallDXCT == "TU") return "Ivory Coast";
			if (CallDXCT == "6Y") return "Jamaica";
			if (CallDXCT == "JX") return "Jan Mayen";
			if (CallDXCT == "JA") return "Japan";
			if (CallDXCT == "JB") return "Japan";
			if (CallDXCT == "JC") return "Japan";
			if (CallDXCT == "JD") return "Japan";
			if (CallDXCT == "JE") return "Japan";
			if (CallDXCT == "JF") return "Japan";
			if (CallDXCT == "JG") return "Japan";
			if (CallDXCT == "JH") return "Japan";
			if (CallDXCT == "JI") return "Japan";
			if (CallDXCT == "JJ") return "Japan";
			if (CallDXCT == "JK") return "Japan";
			if (CallDXCT == "JL") return "Japan";
			if (CallDXCT == "JM") return "Japan";
			if (CallDXCT == "JN") return "Japan";
			if (CallDXCT == "JO") return "Japan";
			if (CallDXCT == "JP") return "Japan";
			if (CallDXCT == "JQ") return "Japan";
			if (CallDXCT == "JS") return "Japan";
			if (CallDXCT == "GJ") return "Jersey";
			if (CallDXCT == "JY") return "Jordan";
			if (CallDXCT == "UN") return "Kazakhstan";
			if (CallDXCT == "UO") return "Kazakhstan";
			if (CallDXCT == "UP") return "Kazakhstan";
			if (CallDXCT == "UQ") return "Kazakhstan";
			if (CallDXCT == "5Z") return "Kenya";
			if (CallDXCT == "5Z") return "Kenya";
			if (CallDXCT == "Z6") return "Kosovo";
			if (CallDXCT == "9K") return "Kuwait";
			if (CallDXCT == "EX") return "Kyrgyzstan";
			if (CallDXCT == "XW") return "Laos";
			if (CallDXCT == "YL") return "Latvia";
			if (CallDXCT == "OD") return "Lebanon";
			if (CallDXCT == "7P") return "Lesotho";
			if (CallDXCT == "EL") return "Liberia";
			if (CallDXCT == "5A") return "Libya";
			if (CallDXCT == "LY") return "Lithuania";
			if (CallDXCT == "LX") return "Luxembourg";
			if (CallDXCT == "Z3") return "Macedonia";
			if (CallDXCT == "5R") return "Madagascar";
			if (CallDXCT == "7Q") return "Malawi";
			if (CallDXCT == "8Q") return "Maldives";
			if (CallDXCT == "TZ") return "Mali";
			if (CallDXCT == "9H") return "Malta";
			if (CallDXCT == "V7") return "Marshall Island";
			if (CallDXCT == "FM") return "Martinique";
			if (CallDXCT == "5T") return "Mauritania";
			if (CallDXCT == "FH") return "Mayotte";
			if (CallDXCT == "XE") return "Mexico";
			if (CallDXCT == "V6") return "Micronesia";
			if (CallDXCT == "JD") return "Minami Torishima";
			if (CallDXCT == "ER") return "Moldova";
			if (CallDXCT == "3A") return "Monaco";
			if (CallDXCT == "JT") return "Mongolia";
			if (CallDXCT == "JU") return "Mongolia";
			if (CallDXCT == "JV") return "Mongolia";
			if (CallDXCT == "4O") return "Montenegro";
			if (CallDXCT == "CN") return "Morocco";
			if (CallDXCT == "C8") return "Mozambique";
			if (CallDXCT == "C9") return "Mozambique";
			if (CallDXCT == "XZ") return "Myanmar (Burma)";
			if (CallDXCT == "V5") return "Namibia";
			if (CallDXCT == "C2") return "Nauru";
			if (CallDXCT == "9N") return "Nepal";
			if (CallDXCT == "PA") return "Netherlands";
			if (CallDXCT == "PB") return "Netherlands";
			if (CallDXCT == "PC") return "Netherlands";
			if (CallDXCT == "PD") return "Netherlands";
			if (CallDXCT == "PE") return "Netherlands";
			if (CallDXCT == "PF") return "Netherlands";
			if (CallDXCT == "PG") return "Netherlands";
			if (CallDXCT == "PH") return "Netherlands";
			if (CallDXCT == "PI") return "Netherlands";
			if (CallDXCT == "PJ") return "Netherlands";
			if (CallDXCT == "FK") return "New Caledonia";
			if (CallDXCT == "ZK") return "New Zealand";
			if (CallDXCT == "ZL") return "New Zealand";
			if (CallDXCT == "ZM") return "New Zealand";
			if (CallDXCT == "5U") return "Niger";
			if (CallDXCT == "5N") return "Nigeria";
			if (CallDXCT == "E6") return "Niue";
			if (CallDXCT == "E5") return "No Cook Island";
			if (CallDXCT == "GI") return "No Ireland";
			if (CallDXCT == "P5") return "No Korea";
			if (CallDXCT == "LA") return "Norway";
			if (CallDXCT == "LB") return "Norway";
			if (CallDXCT == "LC") return "Norway";
			if (CallDXCT == "LD") return "Norway";
			if (CallDXCT == "LE") return "Norway";
			if (CallDXCT == "LF") return "Norway";
			if (CallDXCT == "LG") return "Norway";
			if (CallDXCT == "LH") return "Norway";
			if (CallDXCT == "LI") return "Norway";
			if (CallDXCT == "LJ") return "Norway";
			if (CallDXCT == "LK") return "Norway";
			if (CallDXCT == "LL") return "Norway";
			if (CallDXCT == "LM") return "Norway";
			if (CallDXCT == "LN") return "Norway";
			if (CallDXCT == "JD") return "Ogasawara";
			if (CallDXCT == "A4") return "Oman";
			if (CallDXCT == "AP") return "Pakistan";
			if (CallDXCT == "T8") return "Palau";
			if (CallDXCT == "E4") return "Palestine";
			if (CallDXCT == "HO") return "Panama";
			if (CallDXCT == "HP") return "Panama";
			if (CallDXCT == "P2") return "Papua New Guinea";
			if (CallDXCT == "ZP") return "Paraguay";
			if (CallDXCT == "OA") return "Peru";
			if (CallDXCT == "3Y") return "Peter I";
			if (CallDXCT == "DU") return "Philippines";
			if (CallDXCT == "SN") return "Poland";
			if (CallDXCT == "SM") return "Poland";
			if (CallDXCT == "SO") return "Poland";
			if (CallDXCT == "SP") return "Poland";
			if (CallDXCT == "SQ") return "Poland";
			if (CallDXCT == "SR") return "Poland";
			if (CallDXCT == "CQ") return "Portugal";
			if (CallDXCT == "CR") return "Portugal";
			if (CallDXCT == "CS") return "Portugal";
			if (CallDXCT == "CT") return "Portugal";
			if (CallDXCT == "CU") return "Portugal";
			if (CallDXCT == "XX") return "Portugal";
			if (CallDXCT == "A7") return "Qatar";
			if (CallDXCT == "FR") return "Reunion";
			if (CallDXCT == "TO") return "Reunion";
			if (CallDXCT == "YO") return "Romania";
			if (CallDXCT == "YP") return "Romania";
			if (CallDXCT == "YQ") return "Romania";
			if (CallDXCT == "YR") return "Romania";
			if (CallDXCT == "UA") return "Russia";
			if (CallDXCT == "UB") return "Russia";
			if (CallDXCT == "UC") return "Russia";
			if (CallDXCT == "UD") return "Russia";
			if (CallDXCT == "UE") return "Russia";
			if (CallDXCT == "UF") return "Russia";
			if (CallDXCT == "UG") return "Russia";
			if (CallDXCT == "UH") return "Russia";
			if (CallDXCT == "UI") return "Russia";
			if (CallDXCT == "RA") return "Russia";
			if (CallDXCT == "RB") return "Russia";
			if (CallDXCT == "RC") return "Russia";
			if (CallDXCT == "RD") return "Russia";
			if (CallDXCT == "RE") return "Russia";
			if (CallDXCT == "RF") return "Russia";
			if (CallDXCT == "RG") return "Russia";
			if (CallDXCT == "RH") return "Russia";
			if (CallDXCT == "RI") return "Russia";
			if (CallDXCT == "RJ") return "Russia";
			if (CallDXCT == "RK") return "Russia";
			if (CallDXCT == "RL") return "Russia";
			if (CallDXCT == "RM") return "Russia";
			if (CallDXCT == "RN") return "Russia";
			if (CallDXCT == "RO") return "Russia";
			if (CallDXCT == "RP") return "Russia";
			if (CallDXCT == "RQ") return "Russia";
			if (CallDXCT == "RS") return "Russia";
			if (CallDXCT == "RT") return "Russia";
			if (CallDXCT == "RU") return "Russia";
			if (CallDXCT == "RV") return "Russia";
			if (CallDXCT == "RW") return "Russia";
			if (CallDXCT == "RX") return "Russia";
			if (CallDXCT == "RY") return "Russia";
			if (CallDXCT == "RZ") return "Russia";
			if (CallDXCT == "9X") return "Rwanda";
			if (CallDXCT == "1A") return "Malta";
			if (CallDXCT == "5W") return "Samoa";
			if (CallDXCT == "T7") return "San Marino";
			if (CallDXCT == "S9") return "Sao Tome and Principe";
			if (CallDXCT == "IS") return "Sardinia";
			if (CallDXCT == "HZ") return "Saudi Arabia";
			if (CallDXCT == "8Z") return "Saudi Arabia";
			if (CallDXCT == "7Z") return "Saudi Arabia";
			if (CallDXCT == "GM") return "Scotland";
			if (CallDXCT == "6W") return "Senegal";
			if (CallDXCT == "6V") return "Senegal";
			if (CallDXCT == "YU") return "Serbia";
			if (CallDXCT == "YT") return "Serbia";
			if (CallDXCT == "S7") return "Seychelles";
			if (CallDXCT == "9L") return "Sierra Leone";
			if (CallDXCT == "9V") return "Singapore";
			if (CallDXCT == "S6") return "Singapore";
			if (CallDXCT == "OM") return "Slovakia";
			if (CallDXCT == "S5") return "Slovenia";
			if (CallDXCT == "ZS") return "South Africa";
			if (CallDXCT == "ZR") return "South Africa";
			if (CallDXCT == "ZT") return "South Africa";
			if (CallDXCT == "ZU") return "South Africa"; //Class B, bis 25 Jahre
			if (CallDXCT == "E5") return "South Cook Island";
			if (CallDXCT == "HL") return "South Korea";
			if (CallDXCT == "H4") return "Solomon Island";
			if (CallDXCT == "T5") return "Somalia";
			if (CallDXCT == "6O") return "Somalia";
			if (CallDXCT == "Z8") return "South Sudan";
			if (CallDXCT == "AM") return "Spain";
			if (CallDXCT == "AN") return "Spain";
			if (CallDXCT == "AO") return "Spain";
			if (CallDXCT == "EA") return "Spain";
			if (CallDXCT == "EB") return "Spain";  
			if (CallDXCT == "EC") return "Spain";
			if (CallDXCT == "ED") return "Spain";
			if (CallDXCT == "EE") return "Spain";
			if (CallDXCT == "EF") return "Spain";
			if (CallDXCT == "EG") return "Spain";
			if (CallDXCT == "EH") return "Spain";
			if (CallDXCT == "1S") return "Spratly Island";
			if (CallDXCT == "4P") return "Sri Lanka";
			if (CallDXCT == "4Q") return "Sri Lanka";
			if (CallDXCT == "4R") return "Sri Lanka";
			if (CallDXCT == "4S") return "Sri Lanka";
			if (CallDXCT == "FJ") return "Saint Barthelemy";
			if (CallDXCT == "V4") return "Saint Kitts, Nevis";
			if (CallDXCT == "J6") return "Saint Lucia";
			if (CallDXCT == "FS") return "Saint Martin";
			if (CallDXCT == "FP") return "Saint Pierre and Miquelon";
			if (CallDXCT == "J8") return "Saint Vincent";
			if (CallDXCT == "ST") return "Sudan";
			if (CallDXCT == "PZ") return "Surinam";
			if (CallDXCT == "JW") return "Svalbard";
			if (CallDXCT == "SA") return "Sweden";
			if (CallDXCT == "SB") return "Sweden";
			if (CallDXCT == "SC") return "Sweden";
			if (CallDXCT == "SD") return "Sweden";
			if (CallDXCT == "SE") return "Sweden";
			if (CallDXCT == "SF") return "Sweden";
			if (CallDXCT == "SG") return "Sweden";
			if (CallDXCT == "SH") return "Sweden";
			if (CallDXCT == "SI") return "Sweden";
			if (CallDXCT == "SJ") return "Sweden";
			if (CallDXCT == "SK") return "Sweden";
			if (CallDXCT == "SL") return "Sweden";
			if (CallDXCT == "SM") return "Sweden";
			if (CallDXCT == "7S") return "Sweden";
			if (CallDXCT == "8S") return "Sweden";
			if (CallDXCT == "HB") return "Switzerland";
			if (CallDXCT == "HE") return "Switzerland";
			if (CallDXCT == "YK") return "Syria";
			if (CallDXCT == "6C") return "Syria";
			if (CallDXCT == "BV") return "Taiwan";
			if (CallDXCT == "BM") return "Taiwan"; //Novice-Class
			if (CallDXCT == "BU") return "Taiwan";
			if (CallDXCT == "BW") return "Taiwan";
			if (CallDXCT == "BX") return "Taiwan";
			if (CallDXCT == "5H") return "Tanzania";
			if (CallDXCT == "5I") return "Tanzania";
			if (CallDXCT == "HS") return "Thailand";
			if (CallDXCT == "E2") return "Thailand";
			if (CallDXCT == "4W") return "Timor Leste";
			if (CallDXCT == "5V") return "Togo";
			if (CallDXCT == "A3") return "Tonga";
			if (CallDXCT == "9Y") return "Trinidad and Tobago";
			if (CallDXCT == "9Z") return "Trinidad and Tobago";
			if (CallDXCT == "3V") return "Tunisia";
			if (CallDXCT == "TA") return "Turkey";
			if (CallDXCT == "TB") return "Turkey";
			if (CallDXCT == "TC") return "Turkey";
			if (CallDXCT == "YM") return "Turkey";
			if (CallDXCT == "EZ") return "Turkmenistan";
			if (CallDXCT == "T2") return "Tuvalu";
			if (CallDXCT == "4U") return "UN Headquarter";
			if (CallDXCT == "5X") return "Uganda";
			if (CallDXCT == "A6") return "United Arab Emirates";
			if (CallDXCT == "EM" or CallDXCT == "EN" or CallDXCT == "EO" or CallDXCT == "UR") return "Ukraine";
			if (CallDXCT == "US" or CallDXCT == "UT" or CallDXCT == "UU" or CallDXCT == "UV") return "Ukraine";
			if (CallDXCT == "UW" or CallDXCT == "UX" or CallDXCT == "UY" or CallDXCT == "UY") return "Ukraine";
			if (CallDXCT == "AA" or CallDXCT == "AB" or CallDXCT == "AC" or CallDXCT == "AD") return "United States";
			if (CallDXCT == "AE" or CallDXCT == "AF" or CallDXCT == "AG" or CallDXCT == "AH") return "United States";
			if (CallDXCT == "AI" or CallDXCT == "AJ" or CallDXCT == "AK" or CallDXCT == "AL") return "United States";
			if (CallDXCT == "KA" or CallDXCT == "BB" or CallDXCT == "KC" or CallDXCT == "KD") return "United States";
			if (CallDXCT == "KE" or CallDXCT == "KF" or CallDXCT == "KG" or CallDXCT == "KH") return "United States";
			if (CallDXCT == "KI" or CallDXCT == "KJ" or CallDXCT == "KK" or CallDXCT == "KL") return "United States";
			if (CallDXCT == "KM" or CallDXCT == "KN" or CallDXCT == "KO" or CallDXCT == "KP") return "United States";
			if (CallDXCT == "KQ" or CallDXCT == "KR" or CallDXCT == "KS" or CallDXCT == "KT") return "United States";
			if (CallDXCT == "KU" or CallDXCT == "KV" or CallDXCT == "KW" or CallDXCT == "KX") return "United States";
			if (CallDXCT == "KY" or CallDXCT == "KZ" or CallDXCT == "NA" or CallDXCT == "NB") return "United States";
			if (CallDXCT == "NC" or CallDXCT == "ND" or CallDXCT == "NE" or CallDXCT == "NF") return "United States";
			if (CallDXCT == "NG" or CallDXCT == "NH" or CallDXCT == "NI" or CallDXCT == "NJ") return "United States";
			if (CallDXCT == "NK" or CallDXCT == "NL" or CallDXCT == "NM" or CallDXCT == "NO") return "United States";
			if (CallDXCT == "NP" or CallDXCT == "NQ" or CallDXCT == "NR" or CallDXCT == "NS") return "United States";
			if (CallDXCT == "NT" or CallDXCT == "NU" or CallDXCT == "NV" or CallDXCT == "NW") return "United States";
			if (CallDXCT == "NX" or CallDXCT == "NY" or CallDXCT == "NZ" or CallDXCT == "WA") return "United States";
			if (CallDXCT == "WB" or CallDXCT == "WC" or CallDXCT == "WD" or CallDXCT == "WE") return "United States";
			if (CallDXCT == "WF" or CallDXCT == "WG" or CallDXCT == "WH" or CallDXCT == "WI") return "United States";
			if (CallDXCT == "WJ" or CallDXCT == "WK" or CallDXCT == "WL" or CallDXCT == "WN") return "United States";
			if (CallDXCT == "WM" or CallDXCT == "WO" or CallDXCT == "WP" or CallDXCT == "WQ") return "United States";
			if (CallDXCT == "WR" or CallDXCT == "WS" or CallDXCT == "WT" or CallDXCT == "WU") return "United States";
			if (CallDXCT == "WV" or CallDXCT == "WW" or CallDXCT == "WX" or CallDXCT == "WY") return "United States";
			if (CallDXCT == "WZ") return "United States";
			if (CallDXCT == "CX") return "Uruguay";
			if (CallDXCT == "CV") return "Uruguay";
			if (CallDXCT == "UK") return "Uzbekistan";
			if (CallDXCT == "YJ") return "Vanuatu";
			if (CallDXCT == "HV") return "Vatican";
			if (CallDXCT == "YV") return "Venezuela";
			if (CallDXCT == "3W" or CallDXCT == "XV") return "Vietnam";
			if (CallDXCT == "GW" or CallDXCT == "2W" or CallDXCT == "GC" or CallDXCT == "MW" or CallDXCT == "MC") return "Wales";
			if (CallDXCT == "FW" or CallDXCT == "TX") return "Wallis and Futuna Island";
			if (CallDXCT == "S0") return "Western Sahara";
			if (CallDXCT == "7O") return "Yemen";
			if (CallDXCT == "9I" or CallDXCT == "9J") return "Zambia";
			if (CallDXCT == "Z2") return "Zimbabwe";

			
			
			
			
			return "";
			
			
		}
		
		/** Splittet einen ganzen String in einen durch einen selbst bestimmbaren
			Seperator auf und speichert diesen dann in einem std::vector Array vom Typ String.
			@return std::vector Array vom Typ String.
		*/
		vector<string> Konfiguration::Split(string strSource/**< [in] String der die Daten beinhaltet die geteilt werden sollen. */, string sep/**< [in] Seperator nach dem der String getrennt wird. */) {
			
			char* cstr=const_cast<char*>(strSource.c_str());
			char* current;
			vector<string> arr;
			current=strtok(cstr,sep.c_str());
			while(current!=NULL){
				arr.push_back(current);
				current=strtok(NULL,sep.c_str());
			}
			return arr;
		}
		/** Holt sich die aktuelle Zeit (UTC) aus der Systemuhr.
			@return Ausgabe als Klasse Datum die die Zeit und das Datum enthält..
		*/
		Datum Konfiguration::NowUTC(bool withDate /**< [in] Wenn true, Klasse Datum wird mit Dem Datum gesetzt, andernfalls wird nur die Uhrzeit gesetzt und alle anderen Werte bekommen die default-Parameter. */) const {
			
			time_t Rohtm;
			struct tm * tmstruktur;

			time(&Rohtm);
			
			tmstruktur = gmtime(&Rohtm);
			
			if (withDate == true) return Datum{tmstruktur->tm_mday, tmstruktur->tm_mon, tmstruktur->tm_year + 1900 ,to_string(tmstruktur->tm_hour + 0) + ":" + to_string(tmstruktur->tm_min)}; //UTC
			return Datum{tmstruktur->tm_mday, tmstruktur->tm_mon, tmstruktur->tm_year + 1900}; //UTC
			// return Datum{tmstruktur->tm_hour + 0) + ":" + to_string(tmstruktur->tm_min); //UTC
			
		}
		/** Holt sich die aktuelle Zeit (UTC) aus der Systemuhr.
			@return Ausgabe formatiert in: hh:mm als string.
		*/
		string Konfiguration::NowUTC() const{
			
			time_t Rohtm;
			struct tm * tmstruktur;

			time(&Rohtm);
			
			tmstruktur = gmtime(&Rohtm);
			
			return to_string(tmstruktur->tm_hour + 0) + ":" + to_string(tmstruktur->tm_min); //UTC
			
		}
		/** Holt sich die aktuelle Uhrzeit aus der Systemuhr. @return String im Format "hh:mm".
		*/
		string Konfiguration::Now() const{
			time_t Zeitstempel;
			tm *nun;
			Zeitstempel = time(0);
			nun = localtime(&Zeitstempel);
			return to_string(nun->tm_hour) + ":" + to_string(nun->tm_min);
		}
		
		/** Getter Methode. Holt die Kapazität des Logbuches.
			
		*/
		long Logbuch::GetCapacity() {
			
			Capacity = Entries.max_size();
			return Capacity;
			
		}
		/** Getter Methode. Holt die tatsächliche Anzahl der Einträge im Logbuch.
			
		*/
		long Logbuch::GetCount() {
			Count = Entries.size();
			return Count;
			
		}
		
		/** Holt sich die aktuelle Uhrzeit aus der Systemuhr und konvertiert es in die Klasse Datum.
		
		*/
		Datum Konfiguration::GetDate(bool bolWithYear /**< [in] Wenn true, Klasse Datum wird mit Jahr gesetzt, andernfalls wird 1970 gesetzt. */) const {
			time_t Zeitstempel;
			tm *nun;
			Zeitstempel = time(0);
			nun = localtime(&Zeitstempel);
			
			if (bolWithYear == true) return Datum{nun-> tm_mday, nun-> tm_mon, nun-> tm_year};
			else return Datum{nun-> tm_mday, nun-> tm_mon};
			
		}
		
		
		/** Holt sich die aktuelle Uhrzeit aus der Systemuhr im vollem Format.
		*/
		string Konfiguration::GetDate() const {
			
			
			time_t now;
			now = time(0);
			
			return ctime(&now);

			
		}

		
		/** Speichert das Logbuch.
			Dateiname wird nicht benötigt, wird aus GetFilename() ausgelesen. 
			Gibt bei Erfolg true zurück, andernfalls false.
			
		*/
		bool Logbuch::SaveLogbook()  {
			fstream Puffer;
			Puffer.open("marsch-cmd.log", ios::out);
			if (Puffer) { //Erfolg
				for (Logbucheintrag B : Entries ) /** Ab C++11 forech Schleife: Jeder Eintrag wird durchgegangen um jeden Eintrag zu speichern. */
				{

					// Tabelle TRX Frequenz			Empfangsfrequenz			DatumUTC						Rufzeichen Empfangende Station	Rufzeichen sendende Station
					Puffer << B.GetTRXFrequency() << "," << B.GetRECVFrequency() << "," << B.PrintDateUTC() << "," << B.GetCallReceiver() << "," << B.GetCallSender() << "\n";
				
				
				}
			}
			else return false;
			Puffer.flush();
			Puffer.close(); //Puffer schließen
			return true;
		
		}
		/** Speichert das Logbuch.
			Dateiname wird benötigt. 
			Gibt bei Erfolg true zurück, andernfalls false.
			
		*/
		bool Logbuch::SaveLogbook(string strFilename /**< [in] Zieldateiname ohne Pfadangabe. Es wird versucht das .marsch.cmd Verzeichnis zu nehmen. */)  {
			fstream Puffer;
			Puffer.open(strFilename, ios::out);
			if (Puffer) { //Erfolg
				for (Logbucheintrag B : Entries ) /** Ab C++11 forech Schleife: Jeder Eintrag wird durchgegangen um jeden Eintrag zu speichern. */
				{
				
					// Tabelle TRX Frequenz			Empfangsfrequenz			DatumUTC						Rufzeichen Empfangende Station	Rufzeichen sendende Station
					Puffer << B.GetTRXFrequency() << "," << B.GetRECVFrequency() << "," << B.PrintDateUTC() << "," << B.GetCallReceiver() << "," << B.GetCallSender() << "\n";
				
				
				}
			}
			else return false;
			Puffer.flush();
			Puffer.close(); //Puffer schließen
			return true;
		}
		/** Lädt das Logbuch. (nicht implementiert)
			Dateiname wird benötigt. 
			Gibt bei Erfolg true zurück, andernfalls false.
			
		*/
		bool Logbuch::LoadLogbook(string strFilename /**< [in] Zieldateiname ohne Pfadangabe. Es wird versucht das .marsch.cmd Verzeichnis zu nehmen. */) {
			
			f.open(strFilename, ios::in|ios::out);
			
			f.close();
			
			return true;
			
		}
		/** Konstruktor der Klasse Konfiguration. Diese können in Punkt 1
		der Software im Hauptmenü definiert oder aus einer Konfigurationsdatei
		geladen werden.
			
		*/
		Konfiguration::Konfiguration(string NameSender /**< [in] Der Name der Station. Beispiel: Hans Maier */ , string Timezone, /**< [in] Die Zeitzone der Station. Momentan noch nicht in Verwendung. In den Zeitfunktionen wird die aktuelle Zeitzone vom System zurückgegeben. */ string QTHsender /**< [in] Der Standort der Station.*/) {
			
			this-> NameSender = NameSender; 
			this-> Timezone = Timezone;
			this-> QTHsender = QTHsender;
		}
		/** Destruktor der Klasse Konfiguration. 
			
		*/
		Konfiguration::~Konfiguration() {
			//cout << "Konfiguration entladen." << endl;

		}
		