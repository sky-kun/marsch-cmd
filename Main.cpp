#include <iostream>
#include <string>
#include "Initialisierung.hpp"

/**
* @mainpage marsch-cmd
*
* marsch-cmd ist ein Komandozeilenprogramm das eine ähnliche Funktionalität wie marsch bietet, das auf
Gambas3 Basis geschrieben wurde und eine GUI beinhaltet.
marsch-cmd wurde in C++ geschrieben. Es soll in der Finalen Version möglich sein QSO Logbücher
zu erstellen, kopieren und in das ADIF XML Format zu exportieren.
*  
* <img src="marsch.png" alt="Screenshot"> 
* @author Sky-Kun
* 
*/ 

using namespace std;
/** Hauptfunktion, hier wird dann Start() aufgerufen.
	Gibt bei einem Fehler 1 zurück anstatt 0 bei keinem Fehler.
*/
int main(int argc, char *argv[]) {
	
		if (Start(argc,argv) != 0) {
		
			return 1;
		
		}
	
	return 0;

	
}
