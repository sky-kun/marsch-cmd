#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <streambuf>
#include <string.h>
#include <ctype.h>
#include "Logbuch.hpp"
#include "Own_Exception.hpp"
using namespace std;

/** Hauptauswahl der Funktionen von marsch-cmd.
*/
int Hauptauswahl(Logbuch &Buch) {

	
	//Zeitparameter für Fehlerlog
	// time_t Zeitstempel;
    // tm *nun;
    // Zeitstempel = time(0);
	// nun = localtime(&Zeitstempel);
	
	try {
		//Endlosschleife bzw so lange unendlich bis wir einen Abbruch haben
		bool bolAbbruch = false;
		Datum tempDatum;
		int numAuswahl = 0;
		string strTempAnwahl;
		string strCallSender = "";
		string strNameSender = "";
		
		char TempChar[2];
		
		while (bolAbbruch == false)
		{
			string strTempZwei = "";
			string strTempUhrzeit = "";
			//bool bolErgebnis = false;
			Logbucheintrag tempLog;
			float tempFloat = 0;
			cout << endl << "Hauptauswahl. Aktuelles Logbuch: " << Buch.GetFilename() << endl;
			cout << endl << "Man kann noch " << Buch.GetCapacity() << " weitere Aufzeichnungen eintragen." << endl;
			if (Buch.GetCallSender() == "") cout << "Name der Station:" << " (nicht gesetzt!)" << endl;
			else cout << "Name der Station:" << Buch.GetNameSender() << " (" << Buch.GetCallSender() << ")" << endl;
			cout << "1. Grundlegende Daten des Stationsbetreibers setzen" << endl;
			cout << "2. Eintrag verfassen" << endl;
			cout << "3. Eintrag im Logbuch anzeigen." << endl;
			cout << "4. Gesamtes Logbuch anzeigen." << endl;
			cout << "7. Rufzeichen (DXCC) zu Herkunftsland umwandeln" << endl;
			cout << "8. Aktuelles Datum ausgeben." << endl;
			cout << "9. Programm verlassen" << endl << endl;
			
			cout << "Ihre Auswahl (0-9): ";
			cin >> strTempAnwahl;
			
			char charTemp[1024];
			strcpy(charTemp, strTempAnwahl.c_str());
			
			
			if(isdigit(charTemp[0]) == false) throw Menu_Exception("Buchstabe statt Zahl eingegeben.");
			
			numAuswahl = stoi(strTempAnwahl);
			
			switch(numAuswahl)
			{
				case 0:
					
					cout << "Ungültige Auswahl: " << numAuswahl << endl;
					break;
				case 1:
					//Hier Daten setzen
					cout << "Bitte, Den Namen der Station. Beispielsweise: Hans Maier.: ";
			
					cin >> strTempZwei;
					Buch.SetNameSender(strTempZwei);
					
					cout << "Bitte, Ihren Call eingeben. Beispielsweise: DO9XX.: ";
					
					cin >> strTempZwei;
					Buch.SetCallSender(strTempZwei);
					
					break;
				case 2:

					
					//Zielfrequenz eingeben.
					cout << "Bitte, die Frequenz des Senders eingeben. Format ist bspw. 1.222. Mhz bitte weglassen, es wird von Mhz ausgegangen." << endl;
					cin >> tempFloat;
					tempLog.SetTRXFrequency(tempFloat);
					
					cout << "Bitte, die Frequenz des Empfängers eingeben. Format ist bspw. 1.222. Mhz bitte weglassen, es wird von Mhz ausgegangen." << endl;
					cin >> tempFloat;
					tempLog.SetRECVFrequency(tempFloat);
					
					cout << "Es wird nun die aktuelle Zeit in UTC eingetragen, falls Sie eine andere Zeit als " << Buch.NowUTC() << " eintragen wollen, dann geben Sie jetzt n und dann ENTER ein, andernfalls y und ENTER."<< endl;
					
					cin >> strTempZwei;
					
					if(strTempZwei == "n") {
						
						cout << "Bitte geben Sie das aktuelle Datum im Format TT.MM.JJJJ ein. " << endl;
						cin >> strTempZwei;
						
						cout << "Bitte geben Sie das aktuelle Uhrzeit in UTC im Format ss:mm ein. " << endl;
						cin >> strTempUhrzeit;
						
					}
					else {
						
						//Datum in Logbucheintrag schreiben
						tempLog.SetDateUTC(Buch.NowUTC(true));
					}
					
					//Logbucheintrag hinzufügen und speichern.
					if(Buch.Add(tempLog) == false) cout << "Fehler beim Speichern." << endl;				
					
					//
					break;
				case 3:
					//
					
					break;
				case 4:
					//Alle Einträge anzeigen
					for (Logbucheintrag E : Buch.GetEntries()) {
						
						if (Buch.GetCount() > 0) {
							
							E.Print();
						}
							
						
						
						
						
					}
					
					break;
				case 7:
					//Hier Daten setzen
					cout << "Bitte, Die ersten 2 Stellen des Rufzeichens eingeben: ";
					cin >> strTempZwei;
					
					if (strTempZwei.length() == 2) {
						strcpy(TempChar, strTempZwei.c_str());
						
						Buch.CharArrayUpper(TempChar);
						
						if (Buch.DXCCToCountry(strTempZwei) == "") cout << "Land konnte nicht bestimmt werden." << endl;
						else cout << "Der Prefix " << strTempZwei << " hat das Land " <<  Buch.DXCCToCountry(strTempZwei) << " ergeben." << endl << "Diese Angaben sind ohne Garantie auf Richtigkeit bereitgestellt." << endl;
					}
					else {
						
						cout << "Anzahl der Zeichen entspricht nicht 2." << endl;
						
					}
					break;
				case 8:
					cout << "Aktuelles Datum: " << Buch.GetDate() << endl;
					cout << "Aktuelle Zeit (UTC): " << Buch.NowUTC() << " Uhr" << endl;
					cout << "Aktuelle Zeit: " << Buch.Now() << " Uhr" << endl;
					//
					break;
				case 9:
					//Abbruch...
					bolAbbruch = true;
					break;
				default:
					cout << "Ungültige Auswahl: " << numAuswahl << endl;
					bolAbbruch = true;
					break;
				
			}
		}
		
		return 0;
	}
	catch (const Menu_Exception& e) {
		cout << "Es ist ein Fehler aufgetreten: " << e.what() << endl;
		cout << "Programm kann nicht fortgesetzt werden! " << endl;
		

		return 0;
	}
		
	
}
