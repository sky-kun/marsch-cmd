var class_logbucheintrag =
[
    [ "Logbucheintrag", "class_logbucheintrag.html#ae8b730c3a733585b71ab57d9fdbcc6b5", null ],
    [ "Logbucheintrag", "class_logbucheintrag.html#a5ac836d90a6cf8f2711b62af87189f70", null ],
    [ "GetCallReceiver", "class_logbucheintrag.html#ad053f2d64a3291ec2cdc33dd708c6642", null ],
    [ "GetCallSender", "class_logbucheintrag.html#acbed0fd4a659110a0f29d733aeba605a", null ],
    [ "GetDateUTC", "class_logbucheintrag.html#ac50946a3ab3c5d16052bcf46485cd692", null ],
    [ "GetNameSender", "class_logbucheintrag.html#a23e53cd1308f5cccd4f4c9b6f5a4db01", null ],
    [ "GetOperator", "class_logbucheintrag.html#a728e15c57e41cccbfd2e75a9bbc3c05e", null ],
    [ "GetQSLvia", "class_logbucheintrag.html#a95d2faccccf92ebe48ba6313405a8694", null ],
    [ "GetQTH", "class_logbucheintrag.html#a2102b1be6e798ffdf85843e558615d5a", null ],
    [ "GetTRXFrequency", "class_logbucheintrag.html#abe458ef28afb0182e7fdf42655d74a1d", null ],
    [ "Print", "class_logbucheintrag.html#a90715b3e74b61e4a798d1954f063cc62", null ],
    [ "SetCallReceiver", "class_logbucheintrag.html#a76b4ab85cf75fe042eafa6fa5846d725", null ],
    [ "SetCallSender", "class_logbucheintrag.html#a1e3d91487e22653a6d69851f34c9a4b3", null ],
    [ "SetDateUTC", "class_logbucheintrag.html#ad3b38fb0497dc3ebadc862a5902d5506", null ],
    [ "SetNameReceiver", "class_logbucheintrag.html#a5380d45e1ad534022ae7d0d3798e64a8", null ],
    [ "SetNameSender", "class_logbucheintrag.html#a61c61af9d60c496e148297574fbfd450", null ],
    [ "SetOperator", "class_logbucheintrag.html#ab78920010a56c2123893ffbe8645bdac", null ],
    [ "SetQSLvia", "class_logbucheintrag.html#aea1149396fa7974709fda8fd7618c925", null ],
    [ "SetQTH", "class_logbucheintrag.html#ab5d145286e9837559472245c51dc4de2", null ],
    [ "SetRECVFrequency", "class_logbucheintrag.html#aac49a13538af1011c33aef0bbebf9cee", null ],
    [ "SetTRXFrequency", "class_logbucheintrag.html#a11fe3f7ae0ec1f99322217b9bae85c8d", null ]
];