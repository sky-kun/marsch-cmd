var searchData=
[
  ['getcallreceiver',['GetCallReceiver',['../class_logbucheintrag.html#ad053f2d64a3291ec2cdc33dd708c6642',1,'Logbucheintrag']]],
  ['getcallsender',['GetCallSender',['../class_logbucheintrag.html#acbed0fd4a659110a0f29d733aeba605a',1,'Logbucheintrag']]],
  ['getdateutc',['GetDateUTC',['../class_logbucheintrag.html#ac50946a3ab3c5d16052bcf46485cd692',1,'Logbucheintrag']]],
  ['getdatum',['GetDatum',['../class_datum.html#a1ce2be71cfdc833cd934ceac12a94527',1,'Datum']]],
  ['getfilename',['GetFilename',['../class_logbuch.html#a7c4e43d35e92cc6ed4574a936ec18279',1,'Logbuch']]],
  ['getnamesender',['GetNameSender',['../class_logbucheintrag.html#a23e53cd1308f5cccd4f4c9b6f5a4db01',1,'Logbucheintrag']]],
  ['getoperator',['GetOperator',['../class_logbucheintrag.html#a728e15c57e41cccbfd2e75a9bbc3c05e',1,'Logbucheintrag']]],
  ['getqslvia',['GetQSLvia',['../class_logbucheintrag.html#a95d2faccccf92ebe48ba6313405a8694',1,'Logbucheintrag']]],
  ['getqth',['GetQTH',['../class_logbucheintrag.html#a2102b1be6e798ffdf85843e558615d5a',1,'Logbucheintrag']]],
  ['gettrxfrequency',['GetTRXFrequency',['../class_logbucheintrag.html#abe458ef28afb0182e7fdf42655d74a1d',1,'Logbucheintrag']]]
];
