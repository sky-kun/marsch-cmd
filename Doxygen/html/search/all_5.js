var searchData=
[
  ['loadlogbook',['LoadLogbook',['../class_logbuch.html#a80fdd4a63103b0e4ce5924c3f5038cac',1,'Logbuch']]],
  ['logbuch',['Logbuch',['../class_logbuch.html',1,'Logbuch'],['../class_logbuch.html#a5628288a53c4a6477e77e0ce8f47d8ca',1,'Logbuch::Logbuch()'],['../class_logbuch.html#a48377dfab4994f500361f767a51d4b11',1,'Logbuch::Logbuch(string Filename)']]],
  ['logbuch_2ecpp',['Logbuch.cpp',['../_logbuch_8cpp.html',1,'']]],
  ['logbuch_2ehpp',['Logbuch.hpp',['../_logbuch_8hpp.html',1,'']]],
  ['logbucheintrag',['Logbucheintrag',['../class_logbucheintrag.html',1,'Logbucheintrag'],['../class_logbucheintrag.html#ae8b730c3a733585b71ab57d9fdbcc6b5',1,'Logbucheintrag::Logbucheintrag(string CallSender, string CallReceiver, float TRXFrequency, Datum DateUTC)'],['../class_logbucheintrag.html#a5ac836d90a6cf8f2711b62af87189f70',1,'Logbucheintrag::Logbucheintrag(string CallSender, string CallReceiver, float TRXFrequency, Datum DateUTC, string Mode, string Comment=&quot;&quot;, string NameReceiver=&quot;&quot;, string QRG=&quot;&quot;, string NameSender=&quot;&quot;, string QSLvia=&quot;&quot;, string Operator=&quot;&quot;)']]]
];
