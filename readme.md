# marsch-cmd Alpha Liesmich Datei

13.07.2018 Sky-Kun 
* gitlab@skykun.eu

Marsch steht dabei für "My Amateur Radio Simple Callsign History"

## Beschreibung

marsch-cmd ist ein komandozeilenprogramm das eine ähnliche Funktionalität wie marsch bietet, das auf 
Gambas3 Basis geschrieben wurde und eine GUI beinhaltet.
marsch-cmd wurde in C++ geschrieben. Es soll in der Finalen Version möglich sein QSO Logbücher
zu erstellen, kopieren und in das ADIF XML Format zu exportieren. 


## Win-Support
:zap: Achtung: :zap: Ich gebe keinen Win-Support außer diesen Abschnitt. 
Das das Programm unter M$-Produkten kompiliert werden kann erweckt keinen 
Anspruch auf Support und Hilfestellungen meinerseits. Wenn du ein Issue erstellst bitte diesen Passus beachten. Du wurdest gewarnt! :zap:

English:
:zap: Caution: :zap: Win support is excluded except that text below. Even if you can compile it in M$ products, 
that excites no claim that I give you support for that platform. If you create an issue please remember that. You have been warned! :zap:

Jedoch wenn du das Programm kompilieren möchtest habe ich 
folgendes getestet:

Anyways, if you want to compile the program, I tested that

### Getestet: 
* GNU g++ 6.3.0.1 auf Win 7. 
* Devuan Linux Ascii, GNU g++ 6.3.0

Andere Compiler/Compiler Versionen können, müssen aber nicht funktionieren.
Wie ihr MinGW einrichtet wird beschrieben unter: 
https://www.ics.uci.edu/~pattis/common/handouts/mingweclipse/mingw.html (Englisch)

## Argument zum Kompilieren Linux/Win

`g++ -Wall -Wextra -pedantic-errors -o marsch-cmd Main.cpp Initialisierung.cpp Logbuch.cpp Datum.cpp global_functions.cpp Home_universal.cpp Mkdir_universal.cpp Hauptauswahl.cpp`


## Lizenz

Die Anwendung marsch-cmd und der zugrunde liegende Quelltext 
ist unter der GPL v3 lizenziert, Details in der Datei License.txt 
oder unter https://www.gnu.org/licenses/gpl-3.0.en.html