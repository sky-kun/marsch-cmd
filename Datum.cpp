#include "Datum.hpp"
#include <iostream>
#include <ostream>
#include <string> // string-Datentyp, to_string
using namespace std;

	Datum::Datum() {
	
		t = 1;
		monat = 1;
		jahr = 1970;
		Uhrzeit = "00:00";
		
	}
	Datum::Datum(int Tag, int Monat, int Jahr, string Uhrzeit) {
	
		t = Tag;
		monat = Monat;
		jahr = Jahr;
		this-> Uhrzeit = Uhrzeit;
		
	}
	Datum::Datum(int Tag, int Monat) : t{Tag}, monat{Monat}, jahr{1970}, Uhrzeit{"00:00"} {}
		
	void Datum::Print() const {
	
		cout << t << "." << monat << "." << jahr << endl;
		
	}
	
	string Datum::GetDatum() const {
		
		
		return to_string(t) + "." + to_string(monat) + "." + to_string(jahr) + " " + Uhrzeit; 

	
		
	}
	
	void Datum::SetDatum(int Tag, int Monat, int Jahr, string Uhrzeit) {
		
		t = Tag;
		monat = Monat;
		jahr = Jahr;
		this-> Uhrzeit = Uhrzeit;
	
	}
		
	ostream& operator<< (ostream &out, Datum &sourceDate){
		
		out << sourceDate.GetDatum();
		return out;
		
	}