#ifndef DATUM_HPP
#define DATUM_HPP
#include <string>
#include <iostream>
#include <ostream>
using namespace std;

	class Datum {
		private:
		int t;
		int monat;
		int jahr;
		string Uhrzeit;
		public:
		
		Datum();
		Datum(int Tag, int Monat, int Jahr, string Uhrzeit="00:00");
		Datum(int Tag, int Monat);
		
		void Print() const;
		
		//Setter
		void SetDatum(int Tag, int Monat, int Jahr, string Uhrzeit="00:00");
		//Getter
		string GetDatum() const;
		
		//Operatoren
		friend ostream& operator<< (ostream &out, Datum &sourceDate);
		
	};
	
	
	
	



#endif