#ifndef OWN_EXCEPTION_HPP
#define OWN_EXCEPTION_HPP
#include <string>
#include <exception>

/** Globale Exception Klasse. Fehler ist im Modul Initialisierung aufgetreten. Fehler wird geworfen, wenn durch den Fehler das Programm 
	nicht weiter ausgeführt werden kann.
*/
class Main_Exception : public exception {
	private:
		string error_message;
	public:
		const char* what() const noexcept{ 
		
		return error_message.c_str();
		
		}
	
		Main_Exception(const string &strWhat) : error_message(strWhat) {
			
			//ToDo: Fehlerlogbuch anfertigen und hier abhandeln...
			
		}
	
	
};

/** Globale Exception Klasse. Fehler ist im Modul Hauptauswahl aufgetreten.
*/
class Menu_Exception : public exception {
	private:
		string error_message;
	public:
		const char* what() const noexcept{ 
		
		return error_message.c_str();
		
		}
	
		Menu_Exception(const string &strWhat) : error_message(strWhat) {
			
			//ToDo: Fehlerlogbuch anfertigen und hier abhandeln...
			
		}
	
	
};

/** Globale Exception Klasse. Fehler ist beim Lesen oder Schreiben einer Datei aufgetreten.
*/

class File_Bad_Exception : public exception {
	private:
		string error_message;
		string path;
	public:
		const char* what() const noexcept{ 
		
		return error_message.c_str();
		
		}
	
		File_Bad_Exception(const string &strWhat, const string &strPath) : error_message(strWhat), path(strPath) {
			
			//ToDo: Fehlerlogbuch anfertigen und hier abhandeln...
			
		}
	
	
};

#endif